import lint from "@commitlint/lint";
import load from "@commitlint/load";
import config from "../src/config";

/* the config references the compiled .js files but here we want/need the original .ts files */
config.parserPreset = config.parserPreset.replace(".js", ".ts");
config.formatter = config.formatter.replace(".js", ".ts");

let opts: Awaited<ReturnType<typeof load>>;

beforeAll(async () => {
	opts = await load(config);
});

const validMessages = [
	"fix: lorem ipsum",
	"feat: lorem ipsum",
	"chore(deps): lorem ipsum",
	"refactor!: lorem ipsum",
	"feat(foo)!: lorem ipsum",
];

const invalidMessages = ["lorem ipsum"];

it.each(validMessages)('"%s" should be valid', async (message) => {
	expect.assertions(2);
	const result = await lint(message, opts.rules, {
		parserOpts: opts.parserPreset?.parserOpts,
	});
	expect(result.errors).toEqual([]);
	expect(result.valid).toBeTruthy();
});

it.each(invalidMessages)('"%s" should be invalid', async (message) => {
	expect.assertions(2);
	const result = await lint(message, opts.rules, {
		parserOpts: opts.parserPreset?.parserOpts,
	});
	expect(result.errors).toMatchSnapshot();
	expect(result.valid).toBeFalsy();
});
