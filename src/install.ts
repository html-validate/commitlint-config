/* eslint-disable no-console -- expected to log */

import fs from "node:fs";
import path from "node:path";
import { spawnSync } from "node:child_process";
import isCI from "is-ci";

const MAX_DEPTH = 100;

/**
 * Find the .git directory and return the absolute path.
 *
 * @param cwd - Current working directory
 * @returns Absolute path to git or null if not found.
 */
function findGit(cwd: string): string | null {
	let current = cwd;

	for (let depth = 0; depth < MAX_DEPTH; depth++) {
		const search = path.join(current, ".git");

		if (fs.existsSync(search)) {
			return path.resolve(current);
		}

		/* get the parent directory */
		const child = current;
		current = path.dirname(current);

		/* stop if this is the root directory */
		if (current === child) {
			break;
		}
	}

	return null;
}

/**
 * Calls "git config commit.template .."
 */
function configureCommitTemplate(): void {
	const gitDir = findGit(process.cwd());
	if (!gitDir) {
		console.warn("Failed to locate git directory, skipping gitmessage");
		return;
	}

	const relPath = path.relative(gitDir, __dirname);
	const gitmessage = path.join(relPath, "../gitmessage");
	const args = ["config", "commit.template", gitmessage];
	console.info(`git ${args.join(" ")}`);

	/* eslint-disable-next-line sonarjs/no-os-command-from-path -- intended behaviour */
	spawnSync("git", args);
}

if (!isCI) {
	configureCommitTemplate();
}
