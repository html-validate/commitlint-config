import { fileURLToPath } from "node:url";
import { inspect } from "node:util";
import { type UserConfig } from "@commitlint/types";

import preset from "@commitlint/config-conventional";

const config = {
	...preset,

	parserPreset: fileURLToPath(new URL("parser-preset.js", import.meta.url)),
	formatter: fileURLToPath(new URL("formatter.js", import.meta.url)),

	rules: {
		...preset.rules,
		"header-max-length": [2, "always", 120],
	},
} satisfies UserConfig;

if (process.argv[1] === fileURLToPath(import.meta.url)) {
	const prettyConfig = inspect(config, {
		colors: process.stdout.isTTY,
		depth: 8,
	});
	/* eslint-disable-next-line no-console -- expected to log */
	console.log(prettyConfig);
}

export default config;
