declare module "conventional-changelog-conventionalcommits" {
	function createPreset(config: unknown): unknown;
	export = createPreset;
}
