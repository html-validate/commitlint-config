# @html-validate/commitlint-config changelog

## 3.4.3 (2025-03-09)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.8.0 ([3446cd2](https://gitlab.com/html-validate/commitlint-config/commit/3446cd232510646f9eead1398584c2e7520d519c))

## 3.4.2 (2025-02-16)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.7.1 ([fa7aaf1](https://gitlab.com/html-validate/commitlint-config/commit/fa7aaf15d21bf61bf99ec5c02c5d9ef65ef0fa1f))

## 3.4.1 (2024-12-22)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.6.1 ([6a24a74](https://gitlab.com/html-validate/commitlint-config/commit/6a24a747e1cf0e69a32b685f07a404ae3aadad51))

## 3.4.0 (2024-12-15)

### Features

- **deps:** update dependency is-ci to v4 ([fc1e0b2](https://gitlab.com/html-validate/commitlint-config/commit/fc1e0b2bd2e2f48257dcb048c8b8ba10f786beda))

## 3.3.0 (2024-11-24)

### Features

- **deps:** update dependency conventional-changelog-conventionalcommits to v8 ([314c337](https://gitlab.com/html-validate/commitlint-config/commit/314c33739d61df176315ca8ec37be41e57658c95))

### Bug Fixes

- **deps:** update commitlint monorepo to v19.4.0 ([ca06a9b](https://gitlab.com/html-validate/commitlint-config/commit/ca06a9b1fd5df1278c00c93ab0b464b2a475e818))
- **deps:** update commitlint monorepo to v19.4.1 ([d61a416](https://gitlab.com/html-validate/commitlint-config/commit/d61a41695380f1b590b640c0afd2b6c72ce627e9))
- **deps:** update commitlint monorepo to v19.5.0 ([fe0f84a](https://gitlab.com/html-validate/commitlint-config/commit/fe0f84ac63923a3b8b9676403dafeaa72513569c))
- **deps:** update commitlint monorepo to v19.6.0 ([3ab5021](https://gitlab.com/html-validate/commitlint-config/commit/3ab50213db77d7f58546d437c334c8db84aa121b))

## 3.2.5 (2024-05-12)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.3.0 ([c3e4eda](https://gitlab.com/html-validate/commitlint-config/commit/c3e4edacf79b249e37ec869d9be6b080324392bd))

## 3.2.4 (2024-04-21)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.2.2 ([d9b64c8](https://gitlab.com/html-validate/commitlint-config/commit/d9b64c84177a023b788681ed0c87a4768a4f3cd2))

## 3.2.3 (2024-04-05)

### Bug Fixes

- fix broken `!` for breaking changes ([3d8f052](https://gitlab.com/html-validate/commitlint-config/commit/3d8f0527a412e0191c29d51202d0c131ec504d56))

## 3.2.2 (2024-03-24)

### Bug Fixes

- **deps:** update dependency @commitlint/cli to v19.2.1 ([4c2f831](https://gitlab.com/html-validate/commitlint-config/commit/4c2f831e1c194a2f77e90ecf6eb8fc0d36305401))

## 3.2.1 (2024-03-17)

### Bug Fixes

- **deps:** update commitlint monorepo to v19.1.0 ([b2f98ce](https://gitlab.com/html-validate/commitlint-config/commit/b2f98ce486f9a591bb9b745b0d1d56eaa2095ba4))
- **deps:** update dependency @commitlint/cli to v19.2.0 ([395a18f](https://gitlab.com/html-validate/commitlint-config/commit/395a18f5b6e38c24c69adf060d72453670076d9b))

## 3.2.0 (2024-2-29)

### Features

- **deps:** update commitlint monorepo to v19 ([b8cf4a8](https://gitlab.com/html-validate/commitlint-config/commit/b8cf4a8b655328e4f0316bfbbb49ab5d511903b5))

## 3.1.0 (2024-2-29)

### Features

- precompile binary ([9f4fa97](https://gitlab.com/html-validate/commitlint-config/commit/9f4fa97dd3436ba9c3eba338cacb2e475abeaaf0))

## [3.0.30](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.29...v3.0.30) (2024-2-18)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.6.1 ([85095ba](https://gitlab.com/html-validate/commitlint-config/commit/85095ba17f1eb90cc0c7e1d178af6c4a66195735))
- **deps:** update dependency @commitlint/config-conventional to v18.6.2 ([6cb8355](https://gitlab.com/html-validate/commitlint-config/commit/6cb8355699e29d4861721cab52a0be328e5a9fe1))

## [3.0.29](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.28...v3.0.29) (2024-1-28)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.5.0 ([a462a0a](https://gitlab.com/html-validate/commitlint-config/commit/a462a0ac4e3b5ff97e6c880c60044d690c86629e))
- **deps:** update commitlint monorepo to v18.6.0 ([6bb1256](https://gitlab.com/html-validate/commitlint-config/commit/6bb12566095683eed3a2358573e7d1e06df32496))

## [3.0.28](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.27...v3.0.28) (2024-1-7)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.4.4 ([cfbfe09](https://gitlab.com/html-validate/commitlint-config/commit/cfbfe09f384d84443e67888edc6afc52a889901a))

## [3.0.27](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.26...v3.0.27) (2023-11-26)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.4.3 ([2a61092](https://gitlab.com/html-validate/commitlint-config/commit/2a61092b8737ba2d675825e15c1ef7662d50d780))

## [3.0.26](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.25...v3.0.26) (2023-11-19)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.4.2 ([ebdc598](https://gitlab.com/html-validate/commitlint-config/commit/ebdc59897ac64ec757754d0ad47b61b0ce153cbc))
- **deps:** update dependency @commitlint/cli to v18.4.1 ([c9ee013](https://gitlab.com/html-validate/commitlint-config/commit/c9ee0135c4636ff0708fc0bc34ae6e4ef55ee49f))

## [3.0.25](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.24...v3.0.25) (2023-11-12)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.4.0 ([c925367](https://gitlab.com/html-validate/commitlint-config/commit/c925367e4ba2e0e2a3154862af3217adcfbdaa89))

## [3.0.24](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.23...v3.0.24) (2023-10-29)

### Bug Fixes

- **deps:** update commitlint monorepo to v18.1.0 ([a06033e](https://gitlab.com/html-validate/commitlint-config/commit/a06033eae1dfe1d65ed2ecae37811117455f425d))
- **deps:** update dependency @commitlint/cli to v18.2.0 ([0d8a0ed](https://gitlab.com/html-validate/commitlint-config/commit/0d8a0ede9502d88aff404f39803652b223575646))

## [3.0.23](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.22...v3.0.23) (2023-10-22)

### Bug Fixes

- **deps:** update commitlint monorepo to v18 ([758a1ea](https://gitlab.com/html-validate/commitlint-config/commit/758a1eab96debfbb789ceda8bab123994fb87ea8))

## [3.0.22](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.21...v3.0.22) (2023-10-22)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.8.0 ([1a165ef](https://gitlab.com/html-validate/commitlint-config/commit/1a165eff0ab7e4626d8cb666ab139c83cfccdd6b))
- **deps:** update commitlint monorepo to v17.8.1 ([7443d84](https://gitlab.com/html-validate/commitlint-config/commit/7443d84df8c8b289415c7d27790b3f47f401235b))

## [3.0.21](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.20...v3.0.21) (2023-10-01)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v17.7.2 ([b8d9af9](https://gitlab.com/html-validate/commitlint-config/commit/b8d9af9c463ab2f4fb1dbca7456a1cb6444f337b))

## [3.0.20](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.19...v3.0.20) (2023-08-13)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.7.0 ([8116356](https://gitlab.com/html-validate/commitlint-config/commit/8116356a98c34347d12d6d21f15c2733c353ca28))
- **deps:** update dependency @commitlint/cli to v17.7.1 ([5b00eaf](https://gitlab.com/html-validate/commitlint-config/commit/5b00eafb91d48e0b6c5f6385828a79cd3012940c))

## [3.0.19](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.18...v3.0.19) (2023-07-30)

### Dependency upgrades

- **deps:** require nodejs 16 or later ([61349e5](https://gitlab.com/html-validate/commitlint-config/commit/61349e5fa9146f4afb7b8d769879d5ae0df827ea))

## [3.0.18](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.17...v3.0.18) (2023-07-23)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.6.7 ([3c113a0](https://gitlab.com/html-validate/commitlint-config/commit/3c113a0dd3138b441876e3b6dca37fcd497fb46f))

## [3.0.17](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.16...v3.0.17) (2023-07-02)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.6.6 ([6d56733](https://gitlab.com/html-validate/commitlint-config/commit/6d56733a9574209c3102d4c855c87e470ccc026b))

## [3.0.16](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.15...v3.0.16) (2023-06-04)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.6.5 ([6fd8db1](https://gitlab.com/html-validate/commitlint-config/commit/6fd8db1c451d2afd9f436c4de137ccf7905d092d))

## [3.0.15](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.14...v3.0.15) (2023-05-07)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.6.3 ([0bfe871](https://gitlab.com/html-validate/commitlint-config/commit/0bfe871f0031e20b0da816a3b2e17c8ba9f761c3))

## [3.0.14](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.13...v3.0.14) (2023-04-16)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.6.0 ([ea73802](https://gitlab.com/html-validate/commitlint-config/commit/ea73802d0e14f69e3dcd254c5e28b016c648fb70))
- **deps:** update commitlint monorepo to v17.6.1 ([fca0912](https://gitlab.com/html-validate/commitlint-config/commit/fca0912f2f8a1ae18ed959da8ceeaebc4b1efd83))

## [3.0.13](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.12...v3.0.13) (2023-04-02)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v17.5.1 ([97ff53a](https://gitlab.com/html-validate/commitlint-config/commit/97ff53ab3e18cdd80d4bd2b57332c0efe0b4524f))

## [3.0.12](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.11...v3.0.12) (2023-03-26)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v17.5.0 ([399b6bd](https://gitlab.com/html-validate/commitlint-config/commit/399b6bd6423a4c0de89c651e5d48e7fb256f69ea))

## [3.0.11](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.10...v3.0.11) (2023-02-19)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.4.3 ([7f80ee7](https://gitlab.com/html-validate/commitlint-config/commit/7f80ee7e1827e797f1b7273b6ca8c98d4db4a05e))
- **deps:** update commitlint monorepo to v17.4.4 ([c927a9b](https://gitlab.com/html-validate/commitlint-config/commit/c927a9be020489da91876ffb9d2961dab150d49f))

## [3.0.10](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.9...v3.0.10) (2023-01-22)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.4.2 ([987f1b8](https://gitlab.com/html-validate/commitlint-config/commit/987f1b8d5d02174d14346a05365871f3d98f8bfb))

## [3.0.9](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.8...v3.0.9) (2023-01-08)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.4.0 ([3eeffe7](https://gitlab.com/html-validate/commitlint-config/commit/3eeffe714a92d39c39fd28b47df9a5d6928e97e1))

## [3.0.8](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.7...v3.0.8) (2022-11-27)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.3.0 ([2eaaeb1](https://gitlab.com/html-validate/commitlint-config/commit/2eaaeb1a1fefb60861a4cfff6f50a2700dac2fbd))

## [3.0.7](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.6...v3.0.7) (2022-11-06)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.2.0 ([3f67967](https://gitlab.com/html-validate/commitlint-config/commit/3f67967cada5cc2d98e538888c455d963c3de8f6))

## [3.0.6](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.5...v3.0.6) (2022-09-04)

### Dependency upgrades

- **deps:** update commitlint monorepo ([f51cb6b](https://gitlab.com/html-validate/commitlint-config/commit/f51cb6b07ebfa134edbdbe02ced88364b4c2e7b0))
- **deps:** update dependency @commitlint/cli to v17.1.2 ([93d8338](https://gitlab.com/html-validate/commitlint-config/commit/93d8338642b4e22f76474ed71ef8d9715cae7ab0))

## [3.0.5](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.4...v3.0.5) (2022-07-17)

### Bug Fixes

- allow 120 characters in header ([81400b5](https://gitlab.com/html-validate/commitlint-config/commit/81400b5faa27e2e946a61d9a6ef31869476a0230))

## [3.0.4](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.3...v3.0.4) (2022-07-03)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.0.3 ([fd16be3](https://gitlab.com/html-validate/commitlint-config/commit/fd16be3e02d57db3556f6906bf76af08a8c2c581))

## [3.0.3](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.2...v3.0.3) (2022-06-05)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17.0.2 ([e228ec2](https://gitlab.com/html-validate/commitlint-config/commit/e228ec2461ed1860e7f554630fd542fd4d85fe19))

### [3.0.2](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.1...v3.0.2) (2022-05-29)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v17.0.1 ([0d66f94](https://gitlab.com/html-validate/commitlint-config/commit/0d66f94d7d53c1390f9ddb250d1c638a827af1cb))

### [3.0.1](https://gitlab.com/html-validate/commitlint-config/compare/v3.0.0...v3.0.1) (2022-05-17)

### Dependency upgrades

- **deps:** update commitlint monorepo to v17 ([0941062](https://gitlab.com/html-validate/commitlint-config/commit/09410624980f48043e4949df44d266e89095f87b))
- **deps:** update dependency @commitlint/cli to v16.3.0 ([d91e9e5](https://gitlab.com/html-validate/commitlint-config/commit/d91e9e58f038ffdfa12efaa6e7112a46792e215e))

## [3.0.0](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.10...v3.0.0) (2022-05-15)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([f31b3b5](https://gitlab.com/html-validate/commitlint-config/commit/f31b3b5f3dc7d70a5ed5d8a2669a5eacc48e751b))

### [2.1.10](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.9...v2.1.10) (2022-05-01)

### Dependency upgrades

- **deps:** update commitlint monorepo to v16.2.4 ([906f9e8](https://gitlab.com/html-validate/commitlint-config/commit/906f9e809c95117bbb79433fad13abf87dcf32a0))

### [2.1.9](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.8...v2.1.9) (2022-03-20)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v16.2.3 ([5e838b6](https://gitlab.com/html-validate/commitlint-config/commit/5e838b66e82740220f6b3a6f1b1b57a5f1af678b))

### [2.1.8](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.7...v2.1.8) (2022-02-20)

### Dependency upgrades

- **deps:** update commitlint monorepo to v16.2.1 ([7df9d5d](https://gitlab.com/html-validate/commitlint-config/commit/7df9d5d9f419ad407369ef841b6741de99dcaae2))

### [2.1.7](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.6...v2.1.7) (2022-01-23)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v16.1.0 ([d5ae022](https://gitlab.com/html-validate/commitlint-config/commit/d5ae022619d6cd2d0eff283cedcef7e7a21f0edc))

### [2.1.6](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.5...v2.1.6) (2022-01-16)

### Dependency upgrades

- **deps:** update dependency @commitlint/cli to v16.0.2 ([5d96081](https://gitlab.com/html-validate/commitlint-config/commit/5d960815cc521f4c46955b0df73e333259427742))

### [2.1.5](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.4...v2.1.5) (2022-01-05)

### Dependency upgrades

- **deps:** update commitlint monorepo ([9d78e05](https://gitlab.com/html-validate/commitlint-config/commit/9d78e05eb8b188c6d86657c10fd2e5be917fdf2a))

### [2.1.4](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.3...v2.1.4) (2021-11-21)

### Dependency upgrades

- **deps:** update commitlint monorepo to v15 ([c6f9a09](https://gitlab.com/html-validate/commitlint-config/commit/c6f9a09e3b9c2a9dd6d509e28650d30dc712b00b))

### [2.1.3](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.2...v2.1.3) (2021-11-11)

### Dependency upgrades

- **deps:** update commitlint monorepo to v14 ([abcf26b](https://gitlab.com/html-validate/commitlint-config/commit/abcf26bf2a5061e33e765c59f56f05a02a5ff1ea))

### [2.1.2](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.1...v2.1.2) (2021-10-24)

### Dependency upgrades

- **deps:** pin dependencies ([dbae4d3](https://gitlab.com/html-validate/commitlint-config/commit/dbae4d3bf03b90da80cf85c8e525410796bcdde7))
- **deps:** update dependency @commitlint/config-conventional to v13.2.0 ([0cfc069](https://gitlab.com/html-validate/commitlint-config/commit/0cfc0697c3961d70f77ff03493ca6c220decd9a3))

### [2.1.1](https://gitlab.com/html-validate/commitlint-config/compare/v2.1.0...v2.1.1) (2021-10-17)

### Dependency upgrades

- **deps:** properly depend on `@commitlint/cli` ([b386db2](https://gitlab.com/html-validate/commitlint-config/commit/b386db2c552706a4f40af9c1c213cae68778556b))

## [2.1.0](https://gitlab.com/html-validate/commitlint-config/compare/v2.0.0...v2.1.0) (2021-10-17)

### Features

- wrap `@commitlint/cli` binary ([7bd73b6](https://gitlab.com/html-validate/commitlint-config/commit/7bd73b6c1f1ae93773265e2bd9981d313512fdea))

### Bug Fixes

- use `require.resolve` to find path to extended config ([40a7523](https://gitlab.com/html-validate/commitlint-config/commit/40a752364f33b681988d2bf819df638cddb2fcc1))

## [2.0.0](https://gitlab.com/html-validate/commitlint-config/compare/v1.3.1...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([1a82bdc](https://gitlab.com/html-validate/commitlint-config/commit/1a82bdc4ef86c029134fc6b7d0273247f1f8b320))

### [1.3.1](https://gitlab.com/html-validate/commitlint-config/compare/v1.3.0...v1.3.1) (2021-02-27)

### Bug Fixes

- disable commit template in CI environments ([81ed3ac](https://gitlab.com/html-validate/commitlint-config/commit/81ed3ac1a0e93dd4e5438556557ad5c0c81f0c73))
- resolve path properly instead of relying on `process.cwd()` ([eea67aa](https://gitlab.com/html-validate/commitlint-config/commit/eea67aaeb6622c36fd27ed2f5b1818fa0fdc5bb4))

## [1.3.0](https://gitlab.com/html-validate/commitlint-config/compare/v1.2.0...v1.3.0) (2021-02-21)

### Features

- automatically configure git message ([0c087e9](https://gitlab.com/html-validate/commitlint-config/commit/0c087e910184a61f070ce21a4cf7b7e899013bd0))

### Dependency upgrades

- **deps:** revert `@commitlint/cli` dependency ([644221a](https://gitlab.com/html-validate/commitlint-config/commit/644221a5757600c8eea9a3fbf3dce2eafd167fd0))

## [1.2.0](https://gitlab.com/html-validate/commitlint-config/compare/v1.1.1...v1.2.0) (2020-12-03)

### Features

- include `@commitlint/cli` as dependency ([51baad8](https://gitlab.com/html-validate/commitlint-config/commit/51baad883eb19a9de907b316578e326d64a8ff27))

## [1.1.1](https://gitlab.com/html-validate/commitlint-config/compare/v1.1.0...v1.1.1) (2020-11-01)

### Bug Fixes

- add missing file ([d6428e2](https://gitlab.com/html-validate/commitlint-config/commit/d6428e26a633f45185f4dc4623a4c4f451de38c0))

# [1.1.0](https://gitlab.com/html-validate/commitlint-config/compare/v1.0.3...v1.1.0) (2020-11-01)

### Features

- add `gitmessage` and instructions to use it ([2f8de28](https://gitlab.com/html-validate/commitlint-config/commit/2f8de2890b8b0f7aa4abf23e289748b6dd390ce4))

## [1.0.3](https://gitlab.com/html-validate/commitlint-config/compare/v1.0.2...v1.0.3) (2020-03-30)

## [1.0.2](https://gitlab.com/html-validate/commitlint-config/compare/v1.0.1...v1.0.2) (2020-03-22)

## [1.0.1](https://gitlab.com/html-validate/commitlint-config/compare/v1.0.0...v1.0.1) (2019-12-25)

### Bug Fixes

- use regular dependency ([50c8919](https://gitlab.com/html-validate/commitlint-config/commit/50c8919b354248ea7de2e0b6da6787f9331674a1))

# 1.0.0 (2019-12-25)

### Features

- initial release refactored from html-validate ([f0515cd](https://gitlab.com/html-validate/commitlint-config/commit/f0515cd2ebb43f23396360e134dc523bb9322737))
