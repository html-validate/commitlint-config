# @html-validate/commitlint-config

> HTML-Validate sharable commitlint config.

## Install

```
npm install --save-dev @html-validate/commitlint-config
```

## Usage

In your `package.json` file:

```json
{
  "scripts": {
    "prepare": "git config commit.template ./node_modules/@html-validate/commitlint-config/gitmessage"
  },
  "commitlint": {
    "extends": ["@html-validate"]
  }
}
```
